terraform {
  backend "s3" {
    bucket         = "hashitalks.state"
    dynamodb_table = "hashitalks.state.locking"
    region         = "eu-central-1"
    key            = "app-2/terraform.tfstate"
  }
}